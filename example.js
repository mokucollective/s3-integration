'use strict';
/*
 * This file is an example of how to use the S3 Transfer Client within
 * your own custom code.
 */

/*
 * Require the S3TransferClient class
 */
/** @type {int_s3/S3TransferClient} */
var S3TransferClient = require('int_s3').S3TransferClient;

/*
 * Require the File class
 */
/** @type {dw.io.File} */
var File = require('dw/io/File');

/*
 * Initialize an S3TransferClient instance
 */
/** @type {int_s3.S3TransferClient} */
var myS3Client = new S3TransferClient(
  'mokucollective', /* S3 bucket ID */
  'ABCABCABCABCABC', /* AWS Access Key ID */
  'ab2CFo0B4rB4zD3fH1jKlMN0p', /* AWS Secret Access Key */
  'us-east-1', /* AWS Zone ID */
  'image/png', /* standard MIME types */
  1000, /* Timeout in milliseconds */
  'images' /* Folder within bucket */
  );

/*
 * Initialize a File class instance referring to a local file (may or may not already exist)
 */
/** @type {dw.io.File} */
var localFile = new File(File.IMPEX + '/tmp/image.png');


/*
 * DOWNLOAD EXAMPLE
 */
/** @type {Boolean} */
var downloadSuccess = S3TransferClient.getBinary('MokuLogo.png', localFile);
if (downloadSuccess) {
  // your file downloaded and is now available as the file identified by `localFile`
} else {
  // there was an error and your file was not downloaded, check error logs.
}


/*
 * UPLOAD EXAMPLE
 */
/** @type {Boolean} */
var uploadSuccess = S3TransferClient.putBinary('MokuLogo.png', localFile);
if (uploadSuccess) {
  // your file uploaded and is now available on your S3 bucket as 'MokuLogo.png'
} else {
  // there was an error and your file was not uploaded, check error logs.
}
