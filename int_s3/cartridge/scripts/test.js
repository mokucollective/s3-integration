'use strict';

/**
 * Module that tests the S3 Integration's basic classes
 * 
 * @module int_s3/test
 */

/**
 * Be sure to have set up a testconfig.json in your cartridge or this will fail
 * @see testconfigsample.json for an example.
 */
var config = require('../testconfig.json');
/** @type {int_s3/S3TransferClient} */
var S3TransferClient = require('./lib/S3TransferClient');
/** @type {dw.io.File} */
var File = require('dw/io/File');
/** @type {dw.io.FileWriter} */
var FileWriter = require('dw/io/FileWriter');
/** @type {Object} */
var FileUtils = require('bc_library/cartridge/scripts/io/libFileUtils').FileUtils;

function init(testName) {
	var s3TransferClientInstance = new S3TransferClient(
		config[testName].bucketName,
		config[testName].accessKey,
		config[testName].secretAccessKey,
		config[testName].region,
		config[testName].contentType,
		config[testName].timeout,
		config[testName].remoteFolder
	);
	return s3TransferClientInstance;
}

exports.download = function () {
	var localFile = new File(File.IMPEX + File.SEPARATOR + config['download'].localFilePath);
	FileUtils.ensureFileDirectories(localFile);
	// HTTPClient freaks out if the file exists already
	if(localFile.exists()) {
		localFile.remove();
	}
	var s3TransferClientInstance = init('download');
	var result = s3TransferClientInstance.getBinary(config['download'].remoteFileName, localFile);
	return result;
}

exports.upload = function () {
	var localFile = new File(File.IMPEX + File.SEPARATOR + config['upload'].localFilePath);
	FileUtils.createFileAndFolders(localFile);
	var fileWriter = new FileWriter(localFile, false);
	fileWriter.writeLine("It works!");
	fileWriter.flush();
	fileWriter.close();
	var s3TransferClientInstance = init('upload');
	var result = s3TransferClientInstance.putBinary(config['upload'].remoteFileName, localFile);
	return result && localFile.exists();
}
