# Moku Collective's Amazon S3 Transfer Client for Salesforce Commerce Cloud Digital & Community Suite Integration Framework #
Moku's S3 Transfer Client (STC) enables organizations to take advantage of Amazon's Simple Storage Service (S3) as a bus for communication between Salesforce Commerce Cloud Digital and other disparate systems throughout your enterprise. It is designed to be easily added to existing Integration Framework (IF) installations. It has also been designed to be easily consumed by custom code.

## Assumptions ##
If you intend to use the IF Components provided by this cartridge. This document assumes that the Integration Framework (>=1.5.0) has already been integrated into your codebase.

## Dependencies ##
This library relies upon the `bc_library` cartridge available here: [https://bitbucket.org/demandware/demandware-library]() Version 1.7.0+ is required for this library to function as expected.

## Limitations ##
The STC is limited to transferring files of size no greater than 200mb. This size corresponds to the maximum size of an HTTP Response, as set by Salesforce Commerce Cloud Digital, and can not be modifed within the provided code of the STC.

## License ##
Copyright � 2016 Moku Collective LLC, BootBarn Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Installation & Usage ##
See ReadMe.pdf.

*TODO:* Migrate Installation and Usage instructions to README.md

## Release History ##

- _2016/12/19_ - [1.0.0](https://bitbucket.org/mokucollective/s3-integration/commits/tag/1.0.0)
   - Initial Release

STC uses SemVer (http://semver.org/)

Please use the following version naming schema:

- changes to the 1st digit contain incompatibilities
- changes to the 2nd digit provide new minor features without causing incompatibility
- a change to the 3rd digit provides bug fixes without introducing new features
